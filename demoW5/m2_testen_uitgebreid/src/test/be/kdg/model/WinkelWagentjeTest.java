package be.kdg.model;

import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class WinkelWagentjeTest {
    private WinkelWagentje wagentje;
    private Product product1;
    private Product product2;

    @BeforeEach
    public void setUp() {
//        System.out.println("@BeforeEach");
        wagentje = new WinkelWagentje();
        product1 = new Product(111, "kiwi", 0.45);
        product2 = new Product(222, "banaan", 0.15);
        wagentje.voegToe(product1);
        wagentje.voegToe(product2);
    }

    @AfterEach
    public void tearDown() {
//       System.out.println("@AfterEach");
        wagentje = null;
    }

    @BeforeAll
    public static void hello() {
//        System.out.println("@BeforeAll");
    }

    @AfterAll
    public static void byebye() {
//        System.out.println("@AfterAll");
    }

    @Test
    public void testGevuldWagentje() {
        List<Product> lijst = wagentje.getProductenList();
        assertEquals(2, wagentje.getAantal(), "Het aantal producten moet <2> zijn");
        assertSame(lijst.get(0), product1, "Het eerste product is verkeerd");
        assertEquals(0.60, wagentje.getSaldo(), 0.0, "Het saldo moet <0.60> zijn");
    }

    @Test
    public void testLeegWagentje() {
        wagentje.maakWagentjeLeeg();
        assertEquals(0, wagentje.getAantal(), "Het wagentje moet leeg zijn na methode maakWagentjeLeeg");
        assertEquals(0.0, wagentje.getSaldo(), 0.0, "Het saldo moet <0> zijn na methode maakWagentjeLeeg");
        assertEquals(0, wagentje.getProductenList().size(), "De list met producten moet leeg zijn na methode maakWagentjeLeeg");
    }

    @Test
    public void testVerwijder() {
        wagentje.verwijder(product1);
        assertEquals(1, wagentje.getAantal(), "Het aantal moet <1> zijn na methode verwijder");
        wagentje.verwijder(product2);
        assertEquals(0, wagentje.getAantal(), "Het aantal moet <0> zijn na methode verwijder");
    }

    @Test
    public void testVerwijderNietAanwezig() {
        Product p3 = new Product(666,"Dummy", 4.95);
        assertThrows(IllegalArgumentException.class, () -> wagentje.verwijder(p3),
                "Het verwijderen van een ontbrekend product moet een exception veroorzaken");
    }

    //TODO: werk uit
    public void testUnmodifiable() {

    }
}