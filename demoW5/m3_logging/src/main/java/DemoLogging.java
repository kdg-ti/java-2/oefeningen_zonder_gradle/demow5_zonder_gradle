import be.kdg.model.Product;
import be.kdg.model.WinkelWagentje;
import java.util.logging.LogManager;
import java.io.IOException;
import java.io.InputStream;

public class DemoLogging {
    public static void main(String[] args) {
        loadLoggingConfiguration();

        // 2 foutieve producten aanmaken:
        Product p1 = new Product(1, "", 1.0); //lege naam
        Product p2 = new Product(2, "Koekjes", -5.50); //negatieve prijs

        WinkelWagentje wagentje = new WinkelWagentje();
        wagentje.voegToe(p1);
        wagentje.voegToe(p2);
        wagentje.verwijder(p2);
        wagentje.maakWagentjeLeeg();
    }

    private static void loadLoggingConfiguration() {
        InputStream inputStream = DemoLogging.class.getResourceAsStream("logging.properties");
        try {
            LogManager.getLogManager().readConfiguration(inputStream);
        } catch(IOException e) {
            System.err.println("Logging configuratiebestand kan niet geladen worden");
        }
    }
}
